FROM python:3.10.9

# Create directory for application resources
COPY . /src
WORKDIR src

# Install dependencies
RUN pip install -r dependencies.txt

# Configure container port and UID
EXPOSE 8080
USER 1001

# Run application
CMD ["python", "/src/main.py"] 