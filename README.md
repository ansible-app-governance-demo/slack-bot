# slack-bot

Slack application in Python that allows us to communicate with argoCD and retrieve information from applications and clusters. It also allows you to initiate actions through the Ansible Automation Platform.

## Table of Contents
* [1. About Application Governance Bot](#1-about-application-governance-bot)
  * [1.1 Conversations](#11-conversations)
  * [1.2 Endpoints](#12-endpoints)
* [2. Slack](#2-slack)
  * [2.1 Create Application](#21-create-application)
  * [2.2 Configure Application](#22-configure-application)
  * [2.3 Add application to a channel](#23-add-application-to-a-channel)
  * [2.4 Useful Links](#24-useful-links)
* [3. Run Locally](#3-run-locally)
* [4. Podman](#4-podman)
* [5. OpenShift](#5-openshift)

## 1. About Application Governance Bot

### 1.1 Conversations

- Salute:
  - Input: "**hi|hello|what's up** **app_gov** *[text or empty]*"
  - Output: A salute 
- Applications:
  - Input: "*[text or empty]* **app_gov** *[text or empty]* **applications|apps** *[text or empty]*"
  - Output: The list of applications from ArgoCD
- Clusters:
  - Input: "*[text or empty]* **app_gov** *[text or empty]* **clusters** *[text or empty]*"
  - Output: The list of clusters from ArgoCD
- Move Application:
  - Input: "*[text or empty]* **app_gov** *[text or empty]* **move** *[text or empty]* **application app_name** *[text or empty]*  **cluster cluster_name** *[text or empty]*"
  - Output: Move an application to a different cluster
- Thanks:
  - Input: "*[text or empty]* **thanks** **app_gov** *[text or empty]*"
  - Output: You're welkome message

### 1.2 Endpoints

- GET `/health`: is alive method

- POST `/move-app/update`: method to send an update into Slack regarding an AAP process:

  - Model:

    ```json
    {
    "channel":"",
    "user": "",
    "application":"",
    "cluster":"",
    "jobUrl":"",
    "status":""
    }
    ```

    Available status are `COMPLETED`, `INPROGRESS` and `FAILED`.

  - example:

    ```sh
    curl localhost:3000/move-app/update && \ 
    -d '{"channel":"C04PHKT0139","user":"U04KLTA4MUK","application":"demo-app","cluster":"z-cluster-1","jobUrl":"google.es","status": "FAILED"}' && \
    -k  -H 'Content-Type: application/json
      ```

## 2. Slack

### 2.1 Create Application

1. Go to this [link](https://api.slack.com/apps?new_app=1&ref=bolt_start_hub) to create an application.
2. Select **From Scratch**, add a name (App Governance) and select the desired workspace.
3. Open [Slack API Apps](https://api.slack.com/apps) and select the new one.
4. Go to **Basic Information** > **Display Information** and change:
  - App icon & Preview: select the one in images/jinx.png.
  - Short description: Application to perform governance actions on OpenShift clusters via Ansible Automation Platform.
  - Save changes.

### 2.2 Configure Application

1. Open [Slack API Apps](https://api.slack.com/apps) and select the new one.
2. Go to **OAuth & Permissions** > **Scopes** > **Bot Token Scopes**: Add the 'chat:write' scope to grant your app the permission to post messages in channels it's a member of.
3. Go to **Settings** > **Install App** and click **Install to Workspace**.
4. Go to **Install App** and copy **Bot User OAuth Token** (this value will be provided as an environment variable to the Slack Bot).
5. Go to **Basic Information** and copy **Signing Secret** (this value will be provided as an environment variable to the Slack Bot).
6. Go to **Settings** > **Socket Mode** and enable Socket Mode.
6. Go to **Basic Information** > **App-Level Tokens** and create a token with the scope 'connections:write'.


### 2.3 Add application to a channel

1. Open Slack and select a channel from the workspace where you have installed the application.
2. Open the channel menu and under integrations tab add the recently created application.

### 2.4 Useful Links

- https://api.slack.com/start/building/bolt-python#create
- https://slack.dev/bolt-python/api-docs/slack_bolt/

## 3. Run Locally

Create a virtual environment and load project dependecies:

```sh
# Create environment
python3 -m venv env

# Activate environment
source env/bin/activate

# Load dependencies
pip install -r dependencies.txt
```

!! If you add more dependencies remember to freeze and update dependencies.txt file:

```sh
pip freeze > dependencies.txt
```

Once you've finished stop virtual environment:

```sh
deactivate
```

Open one terminal and run `main.py` with environment variables configured for Salck:

```sh
# Configure OAuth token
export SLACK_BOT_TOKEN=<xoxb-...>

# Configure Signing Secret
export SLACK_SIGNING_SECRET=<signing-secret>

# Configure App token
export SLACK_APP_TOKEN=<xapp-...>

# Configure ArgoCD host url
export ARGOCD_HOST=<https://...>

# Configure ArgoCD admin
export ARGOCD_ADMIN=<admin>

# Configure ArgoCD admin password
export ARGOCD_ADMIN:PASS=<xiskgeua...>

# Configure AAP host url
export AAP_HOST=<https://...>

# Configure AAP user
export AAP_USER=<user>

# Configure AAP pass
export AAP_PASS=<pass>

# Configure AAP pass
export AAP_TEMPLATE_NAME=<template_name>

# Run application
python3 main.py
```

## 4. Podman

Follow this steps to build, run and push the bot image into quay:

```sh
# Build image
podman build -t slack-bot:1.0.0 .

# Tag and push into quay
podman tag slack-bot:1.0.0 quay.io/ansible_helm/slack-bot:1.0.0
podman push quay.io/ansible_helm/slack-bot:1.0.0
```

## 5. OpenShift

```sh
oc apply -f deploy.yaml
```
