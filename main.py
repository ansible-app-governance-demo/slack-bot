
import os
import sys
import re
import logging
from slack_bolt import App
# from slack_bolt.adapter.flask import SlackRequestHandler
from slack_bolt.adapter.socket_mode import SocketModeHandler

logging.basicConfig(level=logging.INFO)

########################################################################################
# Check Environment Variables
########################################################################################
env_var_errors = False
errors = "Required environment variables undefined:"
if "SLACK_BOT_TOKEN" not in os.environ:
  env_var_errors = True
  errors += " SLACK_BOT_TOKEN"
if "SLACK_SIGNING_SECRET" not in os.environ:
  env_var_errors = True
  errors += " SLACK_SIGNING_SECRET"
if "SLACK_APP_TOKEN" not in os.environ:
  env_var_errors = True
  errors += " SLACK_APP_TOKEN"
if "ARGOCD_HOST" not in os.environ:
  env_var_errors = True
  errors += " ARGOCD_HOST"
if "ARGOCD_ADMIN" not in os.environ:
  env_var_errors = True
  errors += " ARGOCD_ADMIN"
if "ARGOCD_ADMIN_PASS" not in os.environ:
  env_var_errors = True
  errors += " ARGOCD_ADMIN_PASS"
if "AAP_USER" not in os.environ:
  env_var_errors = True
  errors += " AAP_USER"
if "AAP_PASS" not in os.environ:
  env_var_errors = True
  errors += " AAP_PASS"
if "AAP_HOST" not in os.environ:
  env_var_errors = True
  errors += " AAP_HOST"
if "AAP_TEMPLATE_NAME" not in os.environ:
  env_var_errors = True
  errors += " AAP_TEMPLATE_NAME"
if env_var_errors:
    sys.exit(errors)


########################################################################################
# Slack Application
########################################################################################
from app.modules import slack

# Initializes your Application with bot token and signing secret
app = App(
  token = os.environ.get("SLACK_BOT_TOKEN"),
  signing_secret = os.environ.get("SLACK_SIGNING_SECRET")
)

@app.message(re.compile("^(hi|hello|what's up).*app_gov*"))
def salute(ack, message, say):
  ack
  slack.salute(message, say)  

@app.message(re.compile(".*app_gov.*(applications|apps)"))
def list_apps(ack, message, say):
  try:
    ack
    slack.list_apps(message, say)
  except Exception as error:
    logging.error(error)
    error_message(message, say, "getting the list of applications")

@app.message(re.compile(".*app_gov.*(clusters)"))
def list_clusters(ack, message, say):
  try:
    ack
    slack.list_clusters(message, say)
  except Exception as error:
    logging.error(error)
    error_message(message, say, "getting the list of clusters")

@app.message(re.compile(".*app_gov\s.*move\s.*(application|app)\s.*cluster\s.*"))
def move_app(ack, message, say):
  try:
    ack
    slack.move_app(message, say)
  except Exception as error:
    logging.error(error)
    error_message(message, say, "requestint the application movement")

@app.message(re.compile(".*thanks.*app_gov*"))
def thanks(ack, message, say):
  ack
  slack.thanks(message, say)  
    
def error_message(message, say, error):
  say( 
    text = "Upsss! There has been an error " + error,
    user = message["channel"]
  )    
            
########################################################################################
# Flask Application (REST endpoints)
########################################################################################

from flask import Flask, request, make_response

flask_app = Flask(__name__)
# handler = SlackRequestHandler(app)
handler = SocketModeHandler(app, os.environ.get("SLACK_APP_TOKEN"))
handler.connect()

@flask_app.route('/health')
def health():
  response = make_response("UP", 200)
  response.mimetype = "text/plain"
  return response

# Input: {"channel":"","user": "","application":"","cluster":"","jobUrl":"","status":""}
# Available status COMPLETED, INPROGRESS, FAILED
@flask_app.route('/move-app/update', methods = ["POST"])
def move_app_update():
  try:
    data = request.json 
    slack.move_app_update(
      app.client, 
      data.get("channel"), 
      data.get("user"), 
      data.get("application"), 
      data.get("cluster"), 
      data.get("jobUrl"),
      data.get("status"))
    response = make_response("UPDATED", 200)
  except Exception as error:
    logging.error(error)
    response = make_response("ERROR", 500)
  response.mimetype = "text/plain"
  return response


# Start Application
if __name__ == "__main__":
  from waitress import serve
  serve(flask_app, host="0.0.0.0", port=8080)
