new_request = [
		{
			"type": "header",
			"text": {
				"type": "plain_text",
				"text": "Request Created",
				"emoji": True
			}
		},
		{
			"type": "section",
			"fields": [
				{
					"type": "mrkdwn",
					"text": "*Type:*\nMove Application"
				},
				{
					"type": "mrkdwn",
					"text": "*Requested by:*\n@calopezb"
				}
			]
		},
		{
			"type": "section",
			"fields": [
				{
					"type": "mrkdwn",
					"text": "*Application:*\ndemo-app"
				},
				{
					"type": "mrkdwn",
					"text": "*Destination:*\ncluster_2"
				}
			]
		},
		{
			"type": "section",
			"fields": [
				{
					"type": "mrkdwn",
					"text": "*Status:*\n:arrows_counterclockwise:  In progress"
				}
			]
		},
		{
			"type": "section",
			"text": {
				"type": "mrkdwn",
				"text": "\n*<google.com|Ansible Automation Platfotm - Workflow>*"
			}
		}
	]