import requests
import os
import json

class ArgoCDAuth(requests.auth.AuthBase):
  def __init__(self, host, admin_pass):
    body = {"username": os.environ.get("ARGOCD_ADMIN"), "password": admin_pass}
    response = requests.post(host + "/api/v1/session", json = body, verify = False)
    self.token = response.json()["token"]       
  def __call__(self, r):
    r.headers["authorization"] = "Bearer " + self.token
    return r
    

########################################################################################
# get available applications
########################################################################################
def get_apps():
  applications = requests.get(
    os.environ.get("ARGOCD_HOST") + "/api/v1/applications?selector=app-type%3Dmanaged",
    auth = ArgoCDAuth(os.environ.get("ARGOCD_HOST"), os.environ.get("ARGOCD_ADMIN_PASS")),
    verify = False
  )
  app_sets = {}

  for item in applications.json()["items"]:
    owner = item["metadata"]["ownerReferences"][0]["name"]
    # TODO: change cluster, now using namespace

    if owner in app_sets:
      app_sets[owner] = app_sets[owner] + ", " + item["spec"]["destination"]["namespace"]

    if owner not in app_sets:
      app_sets[owner] = owner + " in " + item["spec"]["destination"]["namespace"]

      
  return app_sets

########################################################################################
# get available clusters
########################################################################################
def get_clusters():
  clusters = ["cluster_1", "cluster_2", "cluster_3"]
  return clusters
