import requests
import os
from requests.auth import HTTPBasicAuth

  
def move_apps(application, destination, user, channel):
  
  template_id = get_template_id(os.environ.get("AAP_TEMPLATE_NAME"))

  body = {
    "extra_vars": "{" + 
      "'application': '" + application + "'," +
      "'destination': '" + destination + "'," +
      "'user': '" + user + "'," +
      "'channel': '" + channel + "'" +
    "}"
  } 
  response = requests.post(
    os.environ.get("AAP_HOST") + "/api/v2/workflow_job_templates/" + str(template_id) + "/launch/",
    json = body,
    auth = HTTPBasicAuth(os.environ.get("AAP_USER"), os.environ.get("AAP_PASS")),
    verify = False
  )
  # TODO: Raise exception if error
  print(str(response.json()))
  return os.environ.get("AAP_HOST") + "/#/jobs/workflow/" + str(response.json()["id"]) + "/output"

def get_template_id(name):
  response = requests.get(
    os.environ.get("AAP_HOST") + "/api/v2/workflow_job_templates/",
    params={"name": name},
    auth = HTTPBasicAuth(os.environ.get("AAP_USER"), os.environ.get("AAP_PASS")),
    verify = False
  )
  return response.json()["results"][0]["id"]