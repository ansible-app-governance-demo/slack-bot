import json
import copy
import logging
import re
import random
from . import slack_blocks
from . import slack_phrases
from . import argo
from . import aap



logging.basicConfig(level=logging.INFO)

########################################################################################
# salute
########################################################################################
def salute(message, say):
  say( 
    text = "Hi <@" + message["user"] + ">, " + random.choice(slack_phrases.phrases["salute"]),
    user = message["channel"]
  )

########################################################################################
# thanks
########################################################################################
def thanks(message, say):
  say( 
    text = random.choice(slack_phrases.phrases["thanks"]),
    user = message["channel"]
  )
  

########################################################################################
# list applications from Argo (including the cluster)
########################################################################################
def list_apps(message, say):
  say( 
    text = random.choice(slack_phrases.phrases["list_apps"]),
    user = message["channel"]
  )
  applications = argo.get_apps()
  response = "This is the list of applications and the cluster/s where they are deployed: \n"
  for app in applications:
    response += "\n- " + applications[app]
  say( 
    text = response,
    user = message["channel"]
  )

########################################################################################
# list clusters from Argo
########################################################################################
def list_clusters(message, say):
  say( 
    text = random.choice(slack_phrases.phrases["list_clusters"]),
    user = message["channel"]
  )
  clusters = argo.get_clusters()
  response = "This is the list of clusters available right now: \n"
  for cluster in clusters:
    response += "\n- " + cluster
  say( 
    text = response,
    user = message["channel"]
  )

########################################################################################
# move app
########################################################################################
def move_app(message, say):
  say( 
    text = random.choice(slack_phrases.phrases["move_app"]),
    user = message["channel"]
  )
  application = find_word_after(message["text"], "application")
  cluster = find_word_after(message["text"], "cluster")
  job_url = aap.move_apps(application, cluster, message["user"], message["channel"])

  message_block = copy.deepcopy(slack_blocks.new_request)
  message_block[1]["fields"][1]["text"] = "*Requested by:*\n<@" + message["user"] + ">"
  message_block[2]["fields"][0]["text"] = "*Application:*\n" + application
  message_block[2]["fields"][1]["text"] = "*Destination:*\n" + cluster
  message_block[3]["fields"][0]["text"] = "*Status:*\n:arrows_counterclockwise:  In progress"
  message_block[4]["text"]["text"] = "\n*<" + job_url + "|Ansible Automation Platfotm - Workflow Job>*"

  say( 
    blocks = json.dumps(message_block),
    user = message["channel"]
  )

def find_word_after(phrase, key):
  # Find the word after "key"
  match_key = re.search(r"" + key +"\s+(\S+)", phrase)
  if match_key:
    return match_key.group(1)
  else:
    # TODO: error control
    print("No match found")

########################################################################################
# move app update
########################################################################################
def move_app_update(client, channel, user, application, cluster, job_url, status ):

  client.chat_postMessage(
    channel = channel,
    text = random.choice(slack_phrases.phrases["move_app_update"]),
  )

  message_block = copy.deepcopy(slack_blocks.new_request)
  message_block[0]["text"]["text"] = "Request Updated"
  message_block[1]["fields"][1]["text"] = "*Requested by:*\n<@" + user + ">"
  message_block[2]["fields"][0]["text"] = "*Application:*\n" + application
  message_block[2]["fields"][1]["text"] = "*Destination:*\n" + cluster
  message_block[4]["text"]["text"] = "\n*<" + job_url + "|Ansible Automation Platfotm - Workflow Job>*"

  match status:
    case "COMPLETED":
      message_block[3]["fields"][0]["text"] = "*Status:*\n:white_check_mark:  Completed"
    case "INPROGRESS":
      message_block[3]["fields"][0]["text"] = "*Status:*\n:arrows_counterclockwise:  In progress"
    case "FAILED":
      message_block[3]["fields"][0]["text"] = "*Status:*\n:x:  Failed"

  client.chat_postMessage(
    blocks = json.dumps(message_block),
    channel = channel
  )




  
