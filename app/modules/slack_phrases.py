phrases = {
    "salute": [
			":wave: Hey there! If there's anything I can assist you with, just let me know!",
			":star2: Need a hand? I'm here to help! Feel free to reach out if you have any questions or need support",
			":raised_hands: Hey! I'm at your service. If there's anything you need help with, don't hesitate to give me a shout",
			":rainbow: Hi! I'm here to lend a hand. Should you require any assistance, please feel free to inform me",
			":hugging_face: Need assistance? I'm just a message away! Let me know how I can help you out",
			":sparkles: Hey there! If there's anything you need help with, feel free to ask. I'm here for you!",
			":sunflower: Hey! I'm ready to assist you with anything you need. Just give me a heads-up!",
			":tada: Need a friendly helper? I'm here to make things easier for you. Don't hesitate to let me know what you require",
			":star2: Hey! If there's anything I can do to make your life easier, please don't hesitate to reach out",
			":blossom: Need support? I'm all ears! Let me know how I can be of assistance to you"
		],
    "thanks": [
			":+1: Absolutely! I'm here to assist, so feel free to reach out if you need anything",
			":blush: It's my pleasure! Let me know if there's anything else I can do to help",
			":grinning: Glad to be of assistance! Just give me a shout if you require any further support",
			":muscle: Not a problem at all! I'm more than happy to lend a hand whenever you need it",
			":smile: Happy to help! Just let me know what you need, and I'll take care of it",
			":raised_hands: No problem! I'm here to support you, so don't hesitate to ask if there's anything else I can do",
			":star2: It's my pleasure to help! Feel free to reach out if there's anything else I can assist you with",
			":sparkles: Absolutely! I'm here to make your life easier, so don't hesitate to let me know how I can help",
			":sunflower: I'm more than happy to help! Just give me a heads-up if there's anything else you require assistance with",
			":thumbsup: No problem at all! I'm glad I could be of assistance. If there's anything else you need, just let me know"
		],
    "list_apps": [
			":mag_right: Let me take a quick look at our current applications in ArgoCD. I'll be right back in a second...",
			":computer: Just a moment, I'll check the applications in ArgoCD and get back to you in a jiffy...",
			":hourglass_flowing_sand: Allow me a moment to review the applications in ArgoCD. I'll be back with you shortly...",
			":thinking_face: Let me quickly check our current applications in ArgoCD. I'll be back in a moment with the details...",
			":arrows_counterclockwise: Hold on for a second while I investigate the applications in ArgoCD. I'll return shortly with the ","information...",
			":mag: I'll swiftly check the applications in ArgoCD. Just give me a moment, and I'll be right back...",
			":desktop_computer: Let me take a brief look at the applications in ArgoCD. I'll get back to you in a second with the status...",
			":stopwatch: Allow me a quick moment to inspect the applications in ArgoCD. I'll be back shortly to provide you with an update...",
			":briefcase: I'll swiftly verify the current applications in ArgoCD. I'll return in a moment with the information you need...",
			":arrows_clockwise: Hold tight while I quickly check the applications in ArgoCD. I'll be back in a second with the details..."
		],
    "list_clusters": [
			":+1: Absolutely! Let me dive into ArgoCD real quick. Just give me a sec!",
			":mag_right: Sure thing! I'll check in ArgoCD. Hang on for a moment, please!",
			":computer: No problem! I'll take a quick look in ArgoCD. Just give me a sec!",
			":hourglass_flowing_sand: I'm on it! Going to check in ArgoCD. Please wait a moment!",
			":arrows_counterclockwise: Definitely! I'll have a look in ArgoCD. Give me a sec, please!",
			":mag: Sure, I'll dive into ArgoCD. Hold on for a moment, please!",
			":desktop_computer: Absolutely! I'll quickly check in ArgoCD. Just give me a sec!",
			":stopwatch: Not a problem! I'll inspect ArgoCD. Please wait for a moment!",
			":briefcase: I'll take a look in ArgoCD right away. Give me a sec, please!",
			":arrows_clockwise: Sure thing! Going to check in ArgoCD. Please wait a moment!"
		],
    "move_app": [
			":muscle: All right, let's get moving! I'll leverage the Ansible Automation Platform to handle that for us...",
			":rocket: Here we go! I'll make use of the Ansible Automation Platform to take care of that task...",
			":keyboard: Let's dive in! I'm going to engage the Ansible Automation Platform to handle it for us...",
			":gear: Ready, set, go! I'll tap into the power of the Ansible Automation Platform to accomplish that...",
			":computer: Let's make it happen! I'm going to request the Ansible Automation Platform to take care of that for us...",
			":fire: Exciting! I'll trigger the Ansible Automation Platform to handle that task for us...",
			":hammer_and_wrench: Time to take action! I'll leverage the Ansible Automation Platform to make it happen...",
			":zap: Onward we go! I'll utilize the Ansible Automation Platform to handle that task...",
			":bulb: Brilliant! I'll enlist the Ansible Automation Platform to accomplish that for us...",
			":runner: Let's do this! I'll call upon the Ansible Automation Platform to take care of it..."
		],
    "move_app_update": [
			":wave: Hello! It appears that we have received an update from the Ansible Automation Platform:",
			":speech_balloon: Hey there! It looks like we have news from the Ansible Automation Platform:",
			":mega: Greetings! It seems that there is an update from the Ansible Automation Platform:",
			":loudspeaker: Hi there! We have received an update from the Ansible Automation Platform:",
			":envelope_with_arrow: Good day! It appears that we have a new update from the Ansible Automation Platform:",
			":email: Hi! We have just received an update from the Ansible Automation Platform:",
			":bell: Hey! There's an update from the Ansible Automation Platform that we should be aware of:",
			":speech_left: Greetings! It seems we have some information from the Ansible Automation Platform:",
			":bulb: Hello there! We have an update from the Ansible Automation Platform that you should know about:",
			":bookmark_tabs: Hi! We've got an update from the Ansible Automation Platform to share:"
		]
}